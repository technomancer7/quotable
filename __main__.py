import sys, quotable

api = quotable.Quotable()
minLength = None
maxLength = None
tags = None
params = {}
cmd = ""

for index, arg in enumerate(sys.argv[1:]):
    if arg.startswith("--") and ":" in arg:
        params[arg.split(":")[0].replace("--", "")] = arg.split(":")[1]
    elif arg.startswith("--") and ":" not in arg:
        params[arg.replace("--", "")] = True
    else:
        cmd = arg

if cmd == "" or cmd == "random":
    out = api.random(**params)
    print(f'"{out["content"]}" ~{out["author"]}')
if cmd == "quotes":
    out = api.quotes(**params)
    print(out)
