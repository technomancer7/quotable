import requests, json

class Quotable:
    @staticmethod
    def parseOpts(data, possible):
        params = {}
        for k, v in data.items():
            if k in possible:
                params[k] = v
        return params

    @staticmethod
    def random(**opts):
        url = f"https://api.quotable.io/random"
        params = Quotable.parseOpts(opts, ["tags", "maxLength", "minLength"])
        f = requests.get(url, params = params).json()
        return f

    @staticmethod
    def tags(**opts):
        options = ["sortBy", "order"]
        url = f"https://api.quotable.io/tags"
        params = Quotable.parseOpts(opts, options)
        
        f = requests.get(url, params = params).json()
        return f

    @staticmethod
    def authors(**opts):
        options = ["slug", "sortBy", "order", "limit", "page"]
        url = f"https://api.quotable.io/authors"
        params = Quotable.parseOpts(opts, options)
        
        f = requests.get(url, params = params).json()
        return f

    @staticmethod
    def author(author_id):
        url = f"https://api.quotable.io/author/{author_id}"
        f = requests.get(url).json()
        return f

    @staticmethod
    def quotes(**opts):
        options = ["tags", "maxLength", "minLength", "author", "authorId", "sortBy", "order", "limit", "page"]
        url = f"https://api.quotable.io/quotes"
        params = Quotable.parseOpts(opts, options)
        
        f = requests.get(url, params = params).json()
        return f

    @staticmethod
    def quote(quote_id):
        url = f"https://api.quotable.io/quotes/{quote_id}"
        f = requests.get(url).json()
        return f