# quotable
Some stuff for interfacing with [Quotable API](https://github.com/lukePeavey/quotable).

## CLI
```bash
$ python3 quotable
```

## Library
The function names and values you can pass are identical to the options used in the API itself.
The only difference is quotes/:id is quote/ID.
```py
api = quotable.Quotable()
params = {"tags": "technology"}
out = api.random(**params)
print(f'"{out["content"]}" ~{out["author"]}')

```